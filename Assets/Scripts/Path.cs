using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour
{
    public List<GameObject> cities;

    public float pathLength;
    public float pathFermentCount;

    private LineRenderer _line;
    private Constants _constants;

    private void Awake()
    {
        _constants = FindObjectOfType<Constants>();
        
        _line = GetComponent<LineRenderer>();
        _line.SetPositions(new []{Vector3.zero, Vector3.zero});
    }

    public void SetPathLength() => pathLength = 
        Vector3.Distance(cities[0].transform.position, cities[1].transform.position);

    public void SetFermentCount()
    {
        if (pathFermentCount == 0)
            pathFermentCount = _constants.Q / pathLength;
        else
            pathFermentCount = (pathFermentCount + _constants.Q / pathLength) * _constants.Rho;
    }
    
    public void FermentDecrease() => pathFermentCount *= 1 - _constants.Rho;
    
    public void DrawPath()
    {
        if (_line.GetPosition(0) == Vector3.zero || _line.GetPosition(1) == Vector3.zero)
        {
            var positions = new[] {cities[0].transform.position, cities[1].transform.position};
            _line.SetPositions(positions);
        }
        
        SetLineWidth(pathFermentCount);
    }

    private void SetLineWidth(float width)
    {
        _line.startWidth = width / 2;
        _line.endWidth = width / 2;
    }
}
