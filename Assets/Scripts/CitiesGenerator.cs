using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CitiesGenerator : MonoBehaviour
{
    public delegate void CitiesRemoval();

    public static event CitiesRemoval OnCitiesRemoval;
    
    [SerializeField] private int numberOfCities;
    [SerializeField] private GameObject cityPrefab;
    [SerializeField] private Transform citiesParent;
    
    [SerializeField] private Path pathPrefab;
    [SerializeField] private Transform pathsParent;

    [SerializeField] private List<GameObject> cities;
    public List<GameObject> Cities => cities;
   
    [SerializeField] private List<Path> paths;
    public List<Path> Paths => paths;
    
    
    private SettingsUIProvider _settingsUI;

    private const float WidthBound = 9f;
    private const float HeightBound = 4.5f;
    private void Start()
    {
        _settingsUI = FindObjectOfType<SettingsUIProvider>();
        numberOfCities = _settingsUI.GetCitiesNumber();
    }

    private void Update() => numberOfCities = _settingsUI.GetCitiesNumber();

    public void GenerateCities()
    {
        RemoveCities();
        
        for (var i = 0; i < numberOfCities; i++)
        {
            var pointPos = new Vector2(Random.Range(-WidthBound, WidthBound),
                Random.Range(-HeightBound, HeightBound));
            var newPoint =  Instantiate(cityPrefab, pointPos, Quaternion.identity, citiesParent);
            cities.Add(newPoint);
        }

        foreach (var city in cities)
        {
            foreach (var toCity in cities)
            {
                if(toCity == city) continue;
                if (IfPathExist(city, toCity)) continue;
                var newPath =
                    Instantiate(pathPrefab, pathsParent.position, Quaternion.identity, pathsParent);
                newPath.cities.Add(city);
                newPath.cities.Add(toCity);
                newPath.SetPathLength();
                paths.Add(newPath);
            }
        }
    }

    private bool IfPathExist(Object city, Object toCity)
    {
        var isExist = false;
        
        foreach (var path in paths)
        {
            if ((path.cities[0] == city && path.cities[1] == toCity) ||
                (path.cities[0] == toCity && path.cities[1] == city))
                isExist = true;
        }

        return isExist;
    }

    private void RemoveCities()
    {
        foreach (var point in cities)
            Destroy(point);
        cities.Clear();
        
        foreach (var path in paths)
            Destroy(path.gameObject);
        paths.Clear();
        
        OnCitiesRemoval?.Invoke();
    }
}
