using System.Globalization;
using TMPro;
using UnityEngine;

public class SettingsUIProvider : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI numberOfCities;
    [SerializeField] private TMP_InputField antSpeed;
    [SerializeField] private TMP_InputField antsCount;
    [SerializeField] private TextMeshProUGUI alpha;
    [SerializeField] private TextMeshProUGUI beta;
    [SerializeField] private TextMeshProUGUI rho;
    [SerializeField] private TextMeshProUGUI q;
    [SerializeField] private TextMeshProUGUI result;

    private Constants _constants;

    private void Start()
    {
        _constants = FindObjectOfType<Constants>();
        SetConstants();
    }

    public int GetCitiesNumber() => int.Parse(numberOfCities.text);
    public int GetAntsCount() => int.Parse(antsCount.text);
    public float GetAntSpeed() => float.Parse(antSpeed.text);

    public void Increase() => numberOfCities.text = (int.Parse(numberOfCities.text) + 1).ToString();
    public void Decrease() => numberOfCities.text = (int.Parse(numberOfCities.text) - 1).ToString();

    public void SetResult(int index, float resultNumber) => 
        result.text = "Best result: \n" + "Ant with index - " + index + " and distance lenght - " + resultNumber + ".";

    public void CloseApp() => Application.Quit();

    private void SetConstants()
    {
        alpha.text = "Alpha: " + _constants.Alpha.ToString(CultureInfo.CurrentCulture);
        beta.text = "Beta: " + _constants.Beta.ToString(CultureInfo.CurrentCulture);
        rho.text = "Rho: " + _constants.Rho.ToString(CultureInfo.CurrentCulture);
        q.text = "Q: " + _constants.Q.ToString(CultureInfo.CurrentCulture);
    }
}
