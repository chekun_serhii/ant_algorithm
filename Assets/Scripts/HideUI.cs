using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HideUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI buttonSymbol;
    [SerializeField] private List<GameObject> uiFields;

    private bool _isUIShowed = true;

    public void ButtonClick()
    {
        if (_isUIShowed)
            HideUIElements();
        else
            ShowUIElements();
    }

    private void ShowUIElements()
    {
        foreach (var field in uiFields)
            field.SetActive(true);
        _isUIShowed = true;
        buttonSymbol.text = "-";
    }

    private void HideUIElements()
    {
        foreach (var field in uiFields)
            field.SetActive(false);
        _isUIShowed = false;
        buttonSymbol.text = "+";
    }
}
