using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour
{
    [SerializeField] [Range(0.5f, 1f)] private float rho = 0.7f;
    [SerializeField] private float alpha = 1f;
    [SerializeField] private float beta = 2f;
    [SerializeField] private float q = 10f;

    public float Rho => rho;
    public float Alpha => alpha;
    public float Beta => beta;
    public float Q => q;
}
