using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Ant : MonoBehaviour
{
    [SerializeField] private GameObject currentCity;
    [SerializeField] private GameObject destinationCity;

    [SerializeField] private List<GameObject> availableCities;
    [SerializeField] private List<GameObject> visitedCities;

    [SerializeField] private float pathLengthSum;

    [SerializeField] private float speed;

    private CitiesGenerator _citiesGenerator;
    private Constants _constants;
    private SettingsUIProvider _settings;
    private AntsSpawning _antsSpawning;
    private bool _isMoving;
    public float PathLengthSum => pathLengthSum;

    private void Awake()
    {
        _citiesGenerator = FindObjectOfType<CitiesGenerator>();
        _constants = FindObjectOfType<Constants>();
        _settings = FindObjectOfType<SettingsUIProvider>();
        _antsSpawning = FindObjectOfType<AntsSpawning>();

        availableCities = new List<GameObject>();
        foreach (var city in _citiesGenerator.Cities)
            availableCities.Add(city);
    }

    private void Start()
    {
        availableCities.Remove(currentCity);
        visitedCities.Add(currentCity);

        speed = _settings.GetAntSpeed();
        
        StartAntMovement();
    }

    private void Update()
    {
        if (_isMoving)
            transform.position = Vector2.MoveTowards(transform.position,
                destinationCity.transform.position, Time.deltaTime * speed);
    }

    private void StartAntMovement() => StartCoroutine(MoveAnt());
    public void SetCurrentCity(GameObject city) => currentCity = city;

    private IEnumerator MoveAnt()
    {
        if (visitedCities.Count == _citiesGenerator.Cities.Count)
        {
            _antsSpawning.finishedAnts.Add(this);
            yield break;
        }
        
        destinationCity = SetNextCity();
        var currentPath = GetCurrentPath(destinationCity);

        _isMoving = true;

        currentPath.SetFermentCount();
        currentPath.DrawPath();
        
        yield return new WaitUntil(() => transform.position == destinationCity.transform.position);

        _isMoving = false;
        
        currentCity = destinationCity;
        availableCities.Remove(currentCity);
        visitedCities.Add(currentCity);

        pathLengthSum += currentPath.pathLength;

        StartCoroutine(MoveAnt());
    }

    private Path GetCurrentPath(GameObject destination)
    {
        Path currentPath = null;

        foreach (var path in _citiesGenerator.Paths)
            if ((path.cities[0] == currentCity && path.cities[1] == destination) ||
                (path.cities[0] == destination && path.cities[1] == currentCity))
                currentPath = path;

        return currentPath;
    }

    private GameObject SetNextCity()
    {
        List<float> citiesProbability = new List<float>();
        
        float denominator = 0;

        foreach (var city in availableCities)
        {
            Path path = GetCurrentPath(city);
            path.FermentDecrease();
            
            denominator += Mathf.Pow(path.pathFermentCount, _constants.Alpha) * 
                           Mathf.Pow(1 / path.pathLength, _constants.Beta);
        }

        foreach (var city in availableCities)
        {
            var path = GetCurrentPath(city);
            var numerator = Mathf.Pow(path.pathFermentCount, _constants.Alpha) * 
                            Mathf.Pow(1 / path.pathLength, _constants.Beta);
            
            citiesProbability.Add(numerator / denominator);
        }

        return availableCities[FindMaxIndex(citiesProbability)];
    }

    private int FindMaxIndex(List<float> probabilities)
    {
        var index = 0;
        var max = probabilities.Max();

        for (int i = 0; i < probabilities.Count; i++)
        {
            if (probabilities[i] == max)
                index = i;
        }

        return index;
    }
    
}
