using System;
using System.Collections.Generic;
using UnityEngine;

public class AntsSpawning : MonoBehaviour
{
    [SerializeField] private Ant antPrefab;
    [SerializeField] private Transform antParent;
    [SerializeField] private int antCount;

    [SerializeField] private List<Ant> ants;
    
    public List<Ant> finishedAnts;

    private SettingsUIProvider _settings;
    private CitiesGenerator _citiesGenerator;

    private void Start()
    {
        _settings = FindObjectOfType<SettingsUIProvider>();
        _citiesGenerator = FindObjectOfType<CitiesGenerator>();

        CitiesGenerator.OnCitiesRemoval += RemoveAnts;
    }

    private void Update()
    {
        if (finishedAnts.Count != antCount || antCount == 0) return;
        var index = GetFastestAntIndex();
        _settings.SetResult(index, ants[index].PathLengthSum);
    }

    public void StartSimulation()
    {
        antCount = _settings.GetAntsCount();
        
        var count = 0;
        var decrease = 0;
        
        for (int i = 0; i < antCount; i++)
        {
            if (count == _citiesGenerator.Cities.Count)
            {
                decrease += count;
                count = 0;
            }
            else
                count++;
            
            ants.Add(SpawnAnt(_citiesGenerator.Cities[i - decrease]));
        }
    }

    private Ant SpawnAnt(GameObject city)
    {
        var newAnt = Instantiate(antPrefab, city.transform.position, Quaternion.identity, antParent);
        newAnt.SetCurrentCity(city);

        return newAnt;
    }

    private void RemoveAnts()
    {
        foreach (var ant in ants)
            Destroy(ant.gameObject);
        ants.Clear();
    }

    private int GetFastestAntIndex()
    {
        var lowestDistance = ants[0].PathLengthSum;
        var index = 0;

        for (var i = 1; i < ants.Count; i++)
        {
            if (!(lowestDistance > ants[i].PathLengthSum)) continue;
            lowestDistance = ants[i].PathLengthSum;
            index = i;
        }

        return index;
    }
}
